<?php
class Request{
	static private $_current;
	static function current(){
		if (!static::$_current){
			static::$_current = new Request();
		}
		return static::$_current;
	}

	const REQUEST_METHOD_POST = 'post';
	const REQUEST_METHOD_GET = 'get';
	public $method;

	public $post;
	public $query;
	public $files;

	public function __construct(){
		$this->method = static::REQUEST_METHOD_GET;
		if ($_SERVER['REQUEST_METHOD']){
			if (strtolower($_SERVER['REQUEST_METHOD']) === 'post'){
				$this->method = static::REQUEST_METHOD_POST;
				$this->post = new ArrayObject($_POST, ArrayObject::STD_PROP_LIST | ArrayObject::ARRAY_AS_PROP);
			}
		}

		/***
		 *agregar a get el resto de los parametros de la query primero (para que no pisen parametros)
		 *examinar _get[q] para elegir el controlador
		 *parsear caso especial en formato nombre:valor primero
		 *cargar el controlador que corresponde y fijarse si tiene $query_parse
		 *si hay, analizar el resto de q con los nombres pedidos
		 *en cualquier caso, agregarlos tambien como parametros por numero
		 *
		 */

	}

}