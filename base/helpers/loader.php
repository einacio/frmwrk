<?php
static class Loader {
	static public function Controller($file){
		$file ='controllers/'.$file.'.php';
		if (file_exists(APPPATH.$file)){
			include_once APPPATH.$file;
		}elseif(file_exists(BASEPATH.$file)){
			include_once BASEPATH.$file;
		}else{
			throw new Exception('wrong file');
		}
	}
}
